const assert = require('assert');
const helloWorld = require('./index').helloWorld;

describe('Hello World', function() {
  describe('#helloWorld()', function() {
    it('should return "Hello, GitLab!"', function() {
      assert.equal(helloWorld(), 'Hello, GitLab!');
    });
  });
});
